[ ! -d /var/tmp/dbbackups ] && mkdir /var/tmp/dbbackups
backupdir=/var/tmp/dbbackups
docker exec -i database mysqldump -p${PASSWORD} -u root petclinic >$backupdir/petclinic.sql
